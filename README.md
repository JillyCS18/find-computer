#FindComputer

**FindComputer** is an appilcation to help client for selling and buying the components of computer.
From this pandemic era, the people should limit themselves on the public area and reduce the crowd.
Hence, some of stores are temporarily closed. Therefore, the developer hopes that this application
could bring the benefits, not just from the seller who previously could not sell their items, but also
for the consumer who felt so hard to find the component this time.


##Description/Overview

This application will be consisted of ... features:
-   Administration

    > A feature for client to create account and login to user this application

-   Profile
    
    > A feature for client to see their profile data
    
-   Sell Item

    > A feature for client to sell the item 

-   Search

    > A feature for client to search another user


##Requirements

-   Java 8
-   Gradle 5.6.3


##How to Run

-   clone this repository
    ```
    https://gitlab.com/JillyCS18/find-computer.git
    ```
    
-   Open this project in your IDE (Intellij IDEA is recommended)
-   You can tick enable auto-import if you want, but you can do it manually by click "Import changes" in the bottom-right after you opened your project
-   Use default gradle wrapper and click open
-   Wait Intellij IDEA to build the project
-   Go to find-computer/src/main/java/com/jilhamluthfi/findcomputer/FindComputerApplication.java and click run
-   Open localhost:8080

##Author
-   Muhammad Jilham Luthfi