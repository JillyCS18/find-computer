package com.jilhamluthfi.findcomputer.item.service;

import com.jilhamluthfi.findcomputer.item.model.Item;

import java.util.List;

public interface ItemService {

    List<Item> getListOfItemLikeName(String keyword, int ownerId);
    List<Item> getListOfItemByCategory(String category, int ownerId);
    List<Item> getAllItemsByCategory(String category);
    List<Item> getAllItems();

    Item save(Item item);
    Item update(Item item);
    Item getItemById(int id);
    void deleteItem(int id);

}
