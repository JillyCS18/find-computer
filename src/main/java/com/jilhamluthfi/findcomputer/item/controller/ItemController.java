package com.jilhamluthfi.findcomputer.item.controller;

import com.jilhamluthfi.findcomputer.item.model.Item;
import com.jilhamluthfi.findcomputer.item.service.ItemService;
import com.jilhamluthfi.findcomputer.user.model.UserC;
import com.jilhamluthfi.findcomputer.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

@Controller
public class ItemController {

    @Autowired
    private ItemService itemService;

    @Autowired
    private UserService userService;

    @GetMapping(path = "/home/item-list")
    public String item(HttpServletRequest request, Model model) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        model.addAttribute("itemList", user.getItemList());
        model.addAttribute("numberOfItems", user.getItemList().size());
        model.addAttribute("item", new Item());
        return "item";
    }

    @PostMapping(path = "/home/item-list")
    public String item(@ModelAttribute("item") Item item, HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        item.setOwner(user);
        Item savedItem = itemService.save(item);
        user.getItemList().add(savedItem);
        return "redirect:/home/item-list";
    }

    @GetMapping(path = "/home/item-list/{id}")
    public String itemDetail(@PathVariable("id") int id, Model model) {
        Item item = itemService.getItemById(id);
        model.addAttribute("item", item);
        model.addAttribute("newItem", item);
        return "item-detail";
    }

    @PostMapping(path = "/home/item-list/{id}")
    public String itemUpdate(@PathVariable("id") int id, @ModelAttribute("item") Item updatedItem) {
        Item item = itemService.getItemById(id);
        item.setName(updatedItem.getName());
        item.setDescription(updatedItem.getDescription());
        item.setCategory(updatedItem.getCategory());
        item.setPrice(updatedItem.getPrice());
        itemService.update(item);
        return "redirect:/home/item-list/" + id;
    }

    @GetMapping(path = "/home/item-list/delete/{id}")
    public String itemDelete(@PathVariable("id") int id) {
        itemService.deleteItem(id);
        return "redirect:/home/item-list";
    }

    /**
     * Handle search feature and determine the data retrieval according to what was requested.
     * @param pathVarsMap
     * @param model
     * @return the right html file.
     */
    @GetMapping(path = "/search")
    public String search(@RequestParam Map<String, String> pathVarsMap, Model model) {
        String keyword = pathVarsMap.get("keyword");
        String searchBy = pathVarsMap.get("search-by");
        if (searchBy.equals("username")) {
            List<UserC> userList = userService.getListOfUserLikeUsername(keyword);
            model.addAttribute("userList", userList);
            model.addAttribute("numberOfUser", userList.size());
            model.addAttribute("search", keyword);
            return "search";
        } else {
            List<Item> itemList;
            if (searchBy.equals("item")) {
                itemList = itemService.getAllItems();
            } else {
                itemList = itemService.getAllItemsByCategory(keyword);
            }
            model.addAttribute("itemList", itemList);
            model.addAttribute("numberOfItems", itemList.size());
            model.addAttribute("search", keyword);
            return "search-item";
        }
    }

    /**
     * Check if the owner of the item is the same as the authenticated user.
     * @param userItem
     * @param itemId
     * @param request
     * @return redirection to the right url path.
     */
    @GetMapping(path = "/item-detail-check-user/{username}/{id}")
    public String itemByUser(@PathVariable("username") String userItem , @PathVariable("id") int itemId ,HttpServletRequest request) {
        String username = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(username);
        if (user.getUsername().equals(userItem)) {
            return "redirect:/home/item-list/" + itemId;
        } else {
            return "redirect:/" + userItem + "/item-list/" + itemId;
        }
    }

    /**
     * Check if the owner of the item is the same as the authenticated user.
     * @param username
     * @param request
     * @param model
     * @return redirection or the right html file.
     */
    @GetMapping(path = "/{username}")
    public String shop(@PathVariable("username") String username, HttpServletRequest request, Model model) {
        String sessionUsername = (String) request.getSession().getAttribute("username");
        UserC user = userService.findByUsername(sessionUsername);
        if (username.equals(user.getUsername())) {
            return "redirect:/home/profile";
        } else {
            user = userService.findByUsername(username);
            model.addAttribute("user", user);
            model.addAttribute("itemList", user.getItemList());
            model.addAttribute("numberOfItems", user.getItemList().size());
            return "user-shop";
        }
    }

    @GetMapping(path = "/{username}/item-list/{id}")
    public String shopItemDetail(@PathVariable(name = "username") String username, @PathVariable("id") int id, Model model) {
        Item item = itemService.getItemById(id);
        model.addAttribute("item", item);
        return "user-shop-item-detail";
    }

    @GetMapping(path = "/{username}/item-list/delete/{id}")
    public String shopItemDelete(@PathVariable(name = "username") String username, @PathVariable("id") int id) {
        itemService.deleteItem(id);
        return "redirect:/" + username;
    }

    @GetMapping(path = "/{username}/item-list/search")
    public String searchItemByName(@PathVariable(name = "username") String username, @RequestParam(name = "keyword") String keyword, Model model) {
        UserC user = userService.findByUsername(username);
        List<Item> itemList = itemService.getListOfItemLikeName(keyword, user.getId());
        model.addAttribute("user", user);
        model.addAttribute("itemList", itemList);
        model.addAttribute("numberOfItems", itemList.size());
        return "user-shop";
    }

    @GetMapping(path = "/{username}/item-list/filter")
    public String searchAllItemByCategory(@PathVariable(name = "username") String username, @RequestParam(name = "filter-by") String category, Model model) {
        UserC user = userService.findByUsername(username);
        List<Item> itemList = itemService.getListOfItemByCategory(category, user.getId());
        model.addAttribute("user", user);
        model.addAttribute("itemList", itemList);
        model.addAttribute("numberOfItems", itemList.size());
        return "user-shop";
    }
}
