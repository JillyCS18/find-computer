package com.jilhamluthfi.findcomputer.item.model;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;

@Setter
@Getter
@NoArgsConstructor
@Entity
@Table(name = "item")
public class Item {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(name = "id")
    private int id;

    @Column(name = "name")
    private String name;

    @Column(name = "description")
    private String description;

    @Column(name = "category")
    private String category;

    @Column(name = "price")
    private int price;

    @ManyToOne
    @JoinColumn(name = "item_owner_id", referencedColumnName = "id")
    private UserC owner;

}
