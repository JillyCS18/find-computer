package com.jilhamluthfi.findcomputer.item.repository;

import com.jilhamluthfi.findcomputer.item.model.Item;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface ItemRepository extends JpaRepository<Item, Long> {
    Item findById(int id);
    Item findByName(String name);
    List<Item> findAll();
    List<Item> findAllByCategory(String category);

    @Query(value = "select * from item where name like ?1 and item_owner_id = ?2", nativeQuery = true)
    List<Item> findAllItemLikeName(String name, int ownerId);

    @Query(value = "select * from item where category = ?1 and item_owner_id = ?2",
            nativeQuery = true)
    List<Item> findAllItemByCategoryAndOwner(String category, int ownerId);

    @Transactional
    void deleteById(int id);
}
