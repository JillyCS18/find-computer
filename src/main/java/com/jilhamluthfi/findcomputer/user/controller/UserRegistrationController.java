package com.jilhamluthfi.findcomputer.user.controller;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import com.jilhamluthfi.findcomputer.user.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(path = "/register")
public class UserRegistrationController {

    @Autowired
    private UserService userService;

    @ModelAttribute("user")
    public UserC userObject() {
        return new UserC();
    }

    @GetMapping
    public String register() {
        return "register";
    }

    /**
     * Handle the registration activity and check if it was any error happened.
     * @param user
     * @param bindingResult
     * @param model
     * @return the right html file or redirection to the right page if it was any error happened.
     */
    @PostMapping
    public String registerUserAccount(@ModelAttribute("user") UserC user, BindingResult bindingResult, Model model) {
        if (bindingResult.hasErrors()) {
            return "register";
        }
        else if (userService.userIsExisted(user.getUsername())) {
            return "redirect:/register?error";
        }
        else {
            userService.saveUser(user);
            return "redirect:/login?success";
        }
    }
}
