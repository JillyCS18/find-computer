package com.jilhamluthfi.findcomputer.user.service;

import com.jilhamluthfi.findcomputer.user.model.UserC;
import org.springframework.security.core.userdetails.UserDetailsService;

import java.util.List;

public interface UserService extends UserDetailsService {

    List<UserC> getListOfUserLikeUsername(String username);
    UserC findByUsername(String username);
    UserC saveUser(UserC userC);
    UserC updateUser(UserC oldUser, UserC updatedUser);
    boolean userIsExisted(String email);

}
