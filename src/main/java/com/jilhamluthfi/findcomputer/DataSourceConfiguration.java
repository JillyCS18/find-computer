package com.jilhamluthfi.findcomputer;

import javax.sql.DataSource;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class DataSourceConfiguration {

    /**
     * database config.
     * @return datasource
     */
    @Bean
    public DataSource getDataSource() {
        DataSourceBuilder dataSourceBuilder = DataSourceBuilder.create();
        if (System.getenv("PRODUCTION") != null) {
            dataSourceBuilder.url(System.getenv("JDBC_DATABASE_URL"));
            dataSourceBuilder.username(System.getenv("JDBC_DATABASE_USERNAME"));
            dataSourceBuilder.password(System.getenv("JDBC_DATABASE_PASSWORD"));
        } else if (System.getenv("CI") != null) {
            dataSourceBuilder.url("jdbc:h2:mem:testdb");
            dataSourceBuilder.driverClassName("org.h2.Driver");
        } else {
            dataSourceBuilder.url("jdbc:postgresql://localhost:5432/test");
            dataSourceBuilder.username("postgres");
            dataSourceBuilder.password("jillycs152103");
        }
        return dataSourceBuilder.build();
    }
}
